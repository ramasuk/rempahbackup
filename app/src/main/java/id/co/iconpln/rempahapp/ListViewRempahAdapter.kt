package id.co.iconpln.rempahapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide

class ListViewRempahAdapter(val context: Context, val listRempah: ArrayList<Rempah>): BaseAdapter() {
    override fun getView(index: Int, view: View?, viewGroup: ViewGroup?): View {
        val viewLayout = LayoutInflater.from(context).inflate(R.layout.item_list_rempah, viewGroup, false)

        val viewHolder = ViewHolder(viewLayout)
        val rempah = getItem(index) as Rempah
        viewHolder.bind(context, rempah)

        return viewLayout
    }

    override fun getItem(index: Int): Any {
        return listRempah[index]
    }

    override fun getItemId(index: Int): Long {
        return index.toLong()
    }

    override fun getCount(): Int {
        return listRempah.size
    }

    private inner class ViewHolder(view: View){
        private val tvRempahName: TextView = view.findViewById(R.id.tvRempahTitle)
        private val tvRempahDesc: TextView = view.findViewById((R.id.tvRempahDesc))
        private val ivRempahPhoto: ImageView = view.findViewById(R.id.ivRempahImage)

        fun bind(context: Context, rempah: Rempah){
            tvRempahName.text = rempah.name
            tvRempahDesc.text = rempah.desc

            Glide.with(context)
                .load(rempah.photo)
                .into(ivRempahPhoto)
        }

    }

}