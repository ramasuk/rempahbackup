package id.co.iconpln.rempahapp

data class Rempah(
    var name: String = "",
    var desc: String = "",
    var photo: String = "",
    var source: String =""
)