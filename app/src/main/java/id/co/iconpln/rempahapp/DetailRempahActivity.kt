package id.co.iconpln.rempahapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_detail_rempah.*

class DetailRempahActivity : AppCompatActivity() {

    companion object{
        const val EXTRA_NAME = "extra_name"
        const val EXTRA_DESC = "extra_desc"
        const val EXTRA_IMAGE_URL = "extra_image_url"
        const val EXTRA_SOURCE = "extra_source"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_rempah)

        displayRempahDetail()
        setupActionBar()
    }

    private fun setupActionBar() {
        supportActionBar?.title = "Detail Hero"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun displayRempahDetail() {
        tvDetailTitleRempah.text = intent.getStringExtra(EXTRA_NAME)
        tvDetailRempahDesc.text = intent.getStringExtra(EXTRA_DESC)
        tvSourceDetail.text = intent.getStringExtra(EXTRA_SOURCE)
        Glide.with(this)
            .load(intent.getStringExtra(EXTRA_IMAGE_URL))
            .apply {
                (
                        RequestOptions()
                            .centerInside()
                            .placeholder(R.drawable.ic_launcher_background)
                            .error(R.drawable.ic_launcher_foreground)
                        )

            }
            .into(ivDetailPhotoRempah)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            android.R.id.home -> {
                finish()
                true
            }else ->{
                false
            }
        }
    }
}
