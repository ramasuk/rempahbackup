package id.co.iconpln.rempahapp


object RempahsData{

    val listDataRempah: ArrayList<Rempah>
        get() {
            //create empty list rempah
            val list = ArrayList<Rempah>()
            //add elments in data rempah
            for (data in dataRempahs){
                val rempah = Rempah()
                rempah.name = data[0]
                rempah.desc = data[1]
                rempah.photo = data[2]
                rempah.source = data[3]
                list.add(rempah)
            }
            return list
        }

    private var dataRempahs = arrayOf(
        arrayOf(
            "Lada",
            "Lada, disebut juga Merica/Sahang, yang mempunyai nama Latin Piper Albi Linn adalah sebuah tanaman yang kaya akan kandungan kimia, seperti minyak lada, minyak lemak, juga pati. Lada bersifat sedikit pahit, pedas, hangat, dan antipiretik.[1] Tanaman ini sudah mulai ditemukan dan dikenal sejak puluhan abad yang lalu. Pada umumnya orang-orang hanya mengenal lada putih dan lada hitam yang mana sering dimanfaatkan sebagai bumbu dapur.",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Black_Pepper_%28Piper_nigrum%29_fruits.jpg/220px-Black_Pepper_%28Piper_nigrum%29_fruits.jpg",
            "https://id.wikipedia.org/wiki/Lada"
        ),
        arrayOf(
            "Pala",
            "Pala (Myristica fragrans) merupakan tumbuhan berupa pohon yang berasal dari kepulauan Banda, Maluku. Akibat nilainya yang tinggi sebagai rempah-rempah, buah dan biji pala telah menjadi komoditi perdagangan yang penting sejak masa Romawi. Pala disebut-sebut dalam ensiklopedia karya Plinius \"Si Tua\". Semenjak zaman eksplorasi Eropa pala tersebar luas di daerah tropika lain seperti Mauritius dan Karibia (Grenada). Istilah pala juga dipakai untuk biji pala yang diperdagangkan. ",
            "https://upload.wikimedia.org/wikipedia/commons/2/24/Myristica_fragrans_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-097.jpg",
            "https://id.wikipedia.org/wiki/Lada"
        ),
        arrayOf(
            "Kunyit",
            "Kunyit atau kunir, (Curcuma longa Linn. syn. Curcuma domestica Val.), adalah termasuk salah satu tanaman rempah-rempah dan obat asli dari wilayah Asia Tenggara. Tanaman ini kemudian mengalami penyebaran ke daerah Malaysia, Indonesia, Australia bahkan Afrika. Hampir setiap orang Indonesia dan India serta bangsa Asia umumnya pernah mengonsumsi tanaman rempah ini, baik sebagai pelengkap bumbu masakan, jamu atau untuk menjaga kesehatan dan kecantikan. ",
            "https://upload.wikimedia.org/wikipedia/commons/2/2e/Curcuma_longa_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-199.jpg",
            "https://id.wikipedia.org/wiki/Kunyit"
        ),
        arrayOf(
            "Kencur",
            "Kencur (Kaempferia galanga L.) adalah salah satu jenis empon-empon/tanaman obat yang tergolong dalam suku temu-temuan (Zingiberaceae). Rimpang atau rizoma tanaman ini mengandung minyak atsiri dan alkaloid yang dimanfaatkan sebagai stimulan. Nama lainnya adalah cekur (Malaysia) dan pro hom (Thailand). Dalam pustaka internasional (bahasa Inggris) kerap terjadi kekacauan dengan menyebut kencur sebagai lesser galangal (Alpinia officinarum) maupun zedoary (temu putih), yang sebetulnya spesies yang berbeda dan bukan merupakan rempah pengganti. Terdapat pula kerabat dekat kencur yang biasa ditanam di pekarangan sebagai tanaman obat, temu rapet (K. rotunda Jacq.), tetapi mudah dibedakan dari daunnya. ",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/Kencur_Kaempferia_galanga.jpg/450px-Kencur_Kaempferia_galanga.jpg",
            "https://id.wikipedia.org/wiki/Kencur"
        ),
        arrayOf(
            "Jahe",
            "Jahe (Zingiber officinale), adalah tanaman rimpang yang sangat populer sebagai rempah-rempah dan bahan obat. Rimpangnya berbentuk jemari yang menggembung di ruas-ruas tengah. Rasa dominan pedas disebabkan senyawa keton bernama zingeron. ",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Zingiber_officinale_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-146.jpg/435px-Zingiber_officinale_-_K%C3%B6hler%E2%80%93s_Medizinal-Pflanzen-146.jpg",
            "https://id.wikipedia.org/wiki/Jahe"
        ),
        arrayOf(
            "Temulawak",
            "Temu lawak (Curcuma xanthorrhiza) adalah tumbuhan obat yang tergolong dalam suku temu-temuan (Zingiberaceae). Ia berasal dari Indonesia, khususnya Pulau Jawa, kemudian menyebar ke beberapa tempat di kawasan wilayah biogeografi Malesia. Saat ini, sebagian besar budidaya temu lawak berada di Indonesia, Malaysia, Thailand, dan Filipina tanaman ini selain di Asia Tenggara dapat ditemui pula di China, Indochina, Barbados, India, Jepang, Korea, Amerika Serikat dan beberapa negara Eropa.",
            "https://upload.wikimedia.org/wikipedia/id/thumb/2/2a/Rimpang_temu_lawak.jpg/450px-Rimpang_temu_lawak.jpg",
            "https://id.wikipedia.org/wiki/Temulawak"
        ),
        arrayOf(
            "Asam jawa",
            "Asam jawa, asam atau asem adalah sejenis buah yang masam rasanya; biasa digunakan sebagai campuran bumbu dalam banyak masakan Indonesia sebagai perasa atau penambah rasa asam dalam makanan, misalnya pada sayur asam atau kadang-kadang pada kuah pempek. Asam juga digunakan untuk campuran jamu tradisional yang dijual oleh penjual jamu keliling, biasanya ibu-ibu yang menggendong bakul dengan botol berisi aneka jamu (jamu gendong). ",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/0/07/Tamarind_fruit.jpg/384px-Tamarind_fruit.jpg",
            "https://id.wikipedia.org/wiki/Asam_jawa"
        ),
        arrayOf(
            "Kapulaga",
            "Kapulaga adalah sejenis rempah yang dihasilkan dari biji beberapa tanaman dari genera Elettaria dan Amomum dalam keluarga Zingiberaceae (keluarga jahe-jahean). Kedua genera ini adalah tanaman asli Bangladesh, Bhutan, India, Indonesia, Nepal, dan Pakistan; biji kapulaga dapat dikenali dari biji polongnya yang kecil, penampang irisan segitiga, dan berbentuk gelendong kumparan, dengan kulit luar yang tipis, dan biji hitam yang kecil. ",
            "https://ecs7.tokopedia.net/img/cache/700/product-1/2018/9/12/8339128/8339128_3b4b1da1-8ae2-455f-b220-158b8febbbf7_1000_1776.jpg",
            "https://id.wikipedia.org/wiki/Kapulaga"
        ),
        arrayOf(
            "Lengkuas",
            "Lengkuas, laos atau kelawas (Karo) (Alpinia galanga) merupakan jenis tumbuhan umbi-umbian yang bisa hidup di daerah dataran tinggi maupun dataran rendah. Umumnya masyarakat memanfaatkannya sebagai campuran bumbu masak dan pengobatan tradisional. Pemanfaatan lengkuas untuk masakan dengan cara mememarkan rimpang kemudian dicelupkan begitu saja ke dalam campuran masakan, sedangkan untuk pengobatan tradisional yang banyak digunakan adalah lengkuas merah Alpinia purpurata K Schum. ",
            "https://upload.wikimedia.org/wikipedia/commons/thumb/6/65/Gardenology.org-IMG_7562_qsbg11mar.jpg/450px-Gardenology.org-IMG_7562_qsbg11mar.jpg",
            "https://id.wikipedia.org/wiki/Lengkuas"
        ),
        arrayOf(
            "Jintan",
            "Jintan (Trachyspermum roxburghianum syn. Carum roxburghianum) merupakan tumbuhan menjalar yang bijinya dapat digunakan untuk rempah-rempah dan obat-obatan. Biji tanaman ini sering digunakan sebagai bumbu dapur untuk masakan India. Tanaman ini banyak dibudidayakan di India dan Asia Tenggara. Rasanya lebih dekat kepada kelabat atau klabet daripada jenis rempah lain ",
            "https://statik.tempo.co/data/2017/07/04/id_620666/620666_620.jpg",
            "https://id.wikipedia.org/wiki/Jintan"
        )
    )
}